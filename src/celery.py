import os
from celery import Celery
from kombu import Exchange, Queue
from celery.schedules import crontab
from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.production')
app = Celery(
    'src', broker=settings.BROKER_URL
)
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


# BEAT CONFIG

app.conf.beat_schedule = {
    'update-graph-data': {
        'task': 'update_main_graph',
        'schedule': crontab(),
    },
}


# QUEUES CONFIG

default_exchange = Exchange('default', type='direct')

default_queue = Queue(
    'default_queue',
    default_exchange,
    routing_key='default_queue'
)
graphs_queue = Queue(
    'graphs_queue',
    default_exchange,
    routing_key='graphs_queue'
)

app.conf.task_queues = (default_queue, graphs_queue)

app.conf.task_default_queue = 'default_queue'
app.conf.task_default_exchange = 'default_queue'
app.conf.task_default_routing_key = 'default_queue'

app.conf.update(
    CELERYD_POOL_RESTARTS=True,
)
