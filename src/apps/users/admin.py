from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from src.common.utils import custom_titled_filter
from .models import User


@admin.register(User)
class UserAdmin(AuthUserAdmin):
    ordering = ("-date_joined", )
    search_fields = ["email"]
    list_display = ("email", "date_joined",)
    list_display_links = ["email"]
    fieldsets = (
        (None, {
            "fields": (
                "username", "password"
            )
        }),
        ("Personal info", {
            "fields": (
                "first_name", "last_name", "email"
            )
        }),
        ("Permissions", {
            "fields": (
                "is_active", "is_staff", "is_superuser", "groups",
                "user_permissions", "acceptPrivacyPolicy",
                "acceptCommercialNotifications"
            )
        }),
        ("Important dates", {
            "fields": (
                "last_login", "date_joined"
            )
        }),
    )
    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": ("username", "email", "password1", "password2"),
        }),
    )
    list_filter = (
        ("is_active", custom_titled_filter("es activo")),
        ("is_superuser", custom_titled_filter("es superusuario")),
        ("is_staff", custom_titled_filter("es staff")),
    )
