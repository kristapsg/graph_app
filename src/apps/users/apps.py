from django.apps import AppConfig


class UsersAppConfig(AppConfig):
    name = 'src.apps.users'
    verbose_name = 'Users'
