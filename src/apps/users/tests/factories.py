import factory
from django.contrib.auth.hashers import make_password
from ..models import User


class UserFactory(factory.DjangoModelFactory):

    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'User %d' % n)
    email = factory.LazyAttribute(lambda obj: '%s@mail.com' % obj.username)
    password = make_password('secret')
    is_active = True
