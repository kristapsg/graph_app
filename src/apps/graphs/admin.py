from django.contrib import admin
from .models import TestGraph


@admin.register(TestGraph)
class TestGraphAdmin(admin.ModelAdmin):
    list_display = ('created', 'high', 'low')
    ordering = ("-created", )
