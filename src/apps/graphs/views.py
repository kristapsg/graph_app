from django.views.generic import TemplateView
from rest_framework.generics import CreateAPIView, ListAPIView
from .serializers import MainGraphSerializer
from .models import TestGraph


class CreateMainGraph(CreateAPIView):
    serializer_class = MainGraphSerializer


class MainGraphList(ListAPIView):
    serializer_class = MainGraphSerializer

    def get_queryset(self):
        return TestGraph.objects.all().order_by('-created')


class MainGraph(TemplateView):
    template_name = "main_graph.html"
