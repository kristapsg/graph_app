import requests
from src.apps.graphs.serializers import MainGraphSerializer


class GraphManager:
    source_url = "https://api.bittrex.com/v3/markets/BTC-USDT/summary"

    @classmethod
    def get_data(self):
        res = requests.get(self.source_url)
        if res.status_code == 200:
            return res.json()

        return False

    @classmethod
    def update(self):
        data = self.get_data()
        if data:
            ser = MainGraphSerializer(data=data)
            if ser.is_valid(raise_exception=True):
                return ser.save()

        return {}
