from django.urls import path
from .views import MainGraph, CreateMainGraph, MainGraphList


app_name = "graphs"

urlpatterns = [
    path('api/update/', CreateMainGraph.as_view()),
    path('api/list/', MainGraphList.as_view()),

    path('', MainGraph.as_view()),
]
