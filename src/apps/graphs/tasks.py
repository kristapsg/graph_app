from celery.decorators import task
from .utils.update_graph import GraphManager


@task(name="update_main_graph", queue='default_queue')
def update_main_graph():
    res = GraphManager.update()

    return res
