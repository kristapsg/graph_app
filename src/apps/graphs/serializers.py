from rest_framework import serializers
from .models import TestGraph


class MainGraphSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestGraph
        fields = [
            'symbol',
            'high',
            'low',
            'volume',
            'baseVolume',
            'quoteVolume',
            'percentChange',
            'updatedAt'
        ]
