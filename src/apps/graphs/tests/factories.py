import factory
from ..models import TestGraph


class TestGraphFactory(factory.DjangoModelFactory):

    class Meta:
        model = TestGraph

    symbol = factory.Sequence(lambda n: 'Symbol %d' % n)
    high = 1
    low = 2
