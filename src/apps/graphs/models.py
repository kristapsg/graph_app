from django.db import models


class TestGraph(models.Model):
    """
    Field types should be checked in API docs
    """
    created = models.DateTimeField(auto_now_add=True)
    symbol = models.CharField(blank=True, null=True, max_length=20)
    high = models.DecimalField(max_digits=16, decimal_places=8, default=0)
    low = models.DecimalField(max_digits=16, decimal_places=8, default=0)
    volume = models.DecimalField(max_digits=16, decimal_places=8, default=0)
    baseVolume = models.DecimalField(
        max_digits=16, decimal_places=8, default=0
    )
    quoteVolume = models.DecimalField(
        max_digits=16, decimal_places=8, default=0
    )
    percentChange = models.DecimalField(
        max_digits=5, decimal_places=2, default=0
    )
    updatedAt = models.CharField(blank=True, null=True, max_length=25)

    class Meta:
        verbose_name = 'Test Graph'
        verbose_name_plural = 'Test Graphs'

    def __str__(self):
        return f"{self.symbol} - {self.created}"
