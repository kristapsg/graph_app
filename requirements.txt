Django==2.2.12 # LTS
celery==4.4.2
requests==2.23.0
django-cors-headers==3.2.1
django-debug-toolbar==2.2
django-celery-beat==2.0.0
django-celery-results==1.0.1
django-environ==0.4.5
django-jet==1.0.8
djangorestframework==3.11.0
flake8==3.8.1
factory-boy==2.12.0
psycopg2-binary==2.8.5
gunicorn==20.0.4
