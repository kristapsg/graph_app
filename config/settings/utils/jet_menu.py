"""
Run `python manage.py jet_custom_apps_example` to list all models data
"""
JET_SIDE_MENU_CUSTOM_APPS = [
    ('users', [
        'User',
    ]),
    ('graphs', [
        'TestGraph',
    ]),
]
