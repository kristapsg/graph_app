from .common import *
import environ




# ===============================================================
# VARIABLES
# ===============================================================
DEBUG = False
ALLOWED_HOSTS = ["127.0.0.1"]
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True




# ===============================================================
# APPS
# ===============================================================
INSTALLED_APPS += ("gunicorn", )
