from .common import *




# ===============================================================
# VARIABLES
# ===============================================================
ALLOWED_HOSTS = ["*"]
SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False
SECURE_SSL_REDIRECT = False
CORS_ORIGIN_ALLOW_ALL = True




# ===============================================================
# MIDDLEWARE
# ===============================================================
MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)




# ===============================================================
# ADDITIONAL APPS
# ===============================================================
INSTALLED_APPS += ('debug_toolbar', )
