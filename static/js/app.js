
const API_URL = '/api/list/';


var getGraphOptions = (bar1Data, bar2Data, labels) => {
	return {
		xAxis: {
			type: 'category',
			data: labels,
			axisLabel: {
				interval: 0,
				rotate: 30
			}
		},
		yAxis: {
			type: 'value'
		},
		tooltip: {
			trigger: 'axis',
			axisPointer: {
				type: 'shadow'
			}
		},
		grid: { containLabel: true },
		series: [
			{
				name: 'high',
				data: bar1Data,
				type: 'bar',
				showBackground: true,
				backgroundStyle: {
					color: 'rgba(220, 220, 220, 0.8)'
				}
			},
			{
				name: 'low',
				data: bar2Data,
				type: 'bar',
				showBackground: true,
				backgroundStyle: {
					color: 'rgba(220, 220, 220, 0.8)'
				}
			}
		]
	};
}


var getObjectList = (results, field) => {
	return results.map(function (obj) { 
		return obj[field];
	});
}


var showGraph = (results) => {
	var bar1Data = getObjectList(results, "low");
	var bar2Data = getObjectList(results, "high");
	var labels = getObjectList(results, "updatedAt");
	var app = echarts.init(document.getElementById('main'));
	var options = getGraphOptions(bar1Data, bar2Data, labels);
	
	app.setOption(options);
}


var updateGraph = () => {
	fetch(API_URL)
		.then((resp) => resp.json())
		.then(function(data) {
			let results = data.results;
			showGraph(results);
		})
		.catch(function(error) {
			console.log(error);
	});
}


var main = () => {
	updateGraph();
	setInterval(function () {
		updateGraph();
	}, 1000 * 60);
}


main();
