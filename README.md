# Graph App

## Development setup

```bash
# build docker image
docker-compose up --build

# first migrations for custom user modal
docker-compose run app python manage.py makemigrations users

# initial db migrations
docker-compose run app python manage.py migrate

# create superuser
docker-compose run app python manage.py createsuperuser

# populate initial DB if necessary
docker-compose run app python manage.py loaddata db.json
```

## Test

```bash
# unit tests
docker-compose run app python manage.py test

# lint
docker-compose run app flake8
```
