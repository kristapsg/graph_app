
# base image
FROM python:3.8.2

# mantainer
LABEL MANTAINER="Kristaps Goncarovs"

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV APP_HOME=/graph_app

# create workspace folders
RUN mkdir $APP_HOME
RUN mkdir -p $APP_HOME/static
RUN mkdir -p $APP_HOME/media

# create group and user and add user to group
RUN groupadd app && useradd app -g app

# copy proyect to container
COPY . $APP_HOME/

# define workspace
WORKDIR $APP_HOME

# install os dependencies
RUN chmod +x $APP_HOME/scripts/install_os_dependencies.sh
RUN ./scripts/install_os_dependencies.sh install

# update pip
RUN pip install --upgrade pip

# install pip dependencies
RUN pip install -r requirements.txt

# chown all the files to the app user
RUN chown -R app:app $APP_HOME

# change to the app user
USER app
